from selenium.webdriver import Keys
from selenium.webdriver.common.by import By


class ArenaProjectPage:

    def __init__(self, browser):
        self.browser = browser

    def click_dodaj_projekt(self):
        self.browser.find_element(By.CSS_SELECTOR,
                                  "a[href='http://demo.testarena.pl/administration/add_project']").click()

    def search_for_created_project(self, name_of_the_project):
        self.browser.find_element(By.CSS_SELECTOR, "#search").send_keys(name_of_the_project + Keys.ENTER)
