from selenium.webdriver.common.by import By


class SearchResultPage:
    def __init__(self, browser):
        self.browser = browser

    def verify_if_project_created_correctly(self, name_of_the_project):
        assert name_of_the_project in self.browser.find_element(By.CSS_SELECTOR, "td").text
