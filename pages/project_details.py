from selenium.webdriver.common.by import By
from utils.random_message import generate_random_text


class ArenaProjectDetails:

    def __init__(self, browser):
        self.browser = browser

    def add_generated_data_to_new_project(self, name_of_the_project):
        nazwa = self.browser.find_element(By.CSS_SELECTOR, "#name")
        nazwa.send_keys(name_of_the_project)
        self.browser.find_element(By.CSS_SELECTOR, "#prefix").send_keys(generate_random_text(3))
        self.browser.find_element(By.CSS_SELECTOR, "#description").send_keys(generate_random_text(50))

    def click_zapisz(self):
        self.browser.find_element(By.CSS_SELECTOR, "input#save").click()
