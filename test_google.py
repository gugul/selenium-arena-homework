import time

from selenium.webdriver import Chrome, Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.support.wait import WebDriverWait
from webdriver_manager.chrome import ChromeDriverManager


def test_searching_in_google():
    browser = Chrome(executable_path=ChromeDriverManager().install())
    browser.set_window_size(1920, 1080)
    browser.get("https://google.com")
    # Wait for cookies
    wait = WebDriverWait(browser, 10)
    element_to_wait_for = (By.CSS_SELECTOR, "#L2AGLb div")
    wait.until(expected_conditions.visibility_of_element_located(element_to_wait_for))
    # Akceptacja ciastka
    cookie_accept_button = browser.find_element(By.CSS_SELECTOR, "#L2AGLb div")
    cookie_accept_button.click()
    # Znalezienie paska wyszukiwania
    search_input = browser.find_element(By.CSS_SELECTOR, "#APjFqb")
    # Asercje że elementy są widoczne dla użytkownika
    assert search_input.is_displayed()
    # Szukanie 4testers
    search_input.send_keys("science")
    search_input.submit()
    element_to_wait_for1 = browser.find_element(By.CSS_SELECTOR, "h3.LC20lb")
    wait.until(expected_conditions.element_to_be_clickable(element_to_wait_for1))
    # Sprawdzenie że jakikolwiek wynik ma tytuł '4testers'
    results = browser.find_elements(By.CSS_SELECTOR, "h3.LC20lb")
    # lista tytułów
    list_of_titles = []
    for i in results:
        list_of_titles.append(i.text)
    # Asercja
    assert "Science | AAAS" in list_of_titles
    # Zamknięcie programu
    browser.quit
