from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By


def test_user_can_login():
    browser = Chrome()
    browser.set_window_size(1920, 1080)
    browser.get("https://www.saucedemo.com")
    username_input = browser.find_element(By.CSS_SELECTOR, "#user-name")
    assert username_input.is_displayed()
    password_input = browser.find_element(By.CSS_SELECTOR, "#password")
    assert password_input.is_displayed()
    login_button = browser.find_element(By.CSS_SELECTOR, "#login-button")
    assert login_button.is_displayed()
    username_input.send_keys("standard_user")
    password_input.send_keys("secret_sauce")
    login_button.click()
    new_page_confirmation_element = browser.find_element(By.CSS_SELECTOR, "span.title")
    assert new_page_confirmation_element.is_displayed()

